Title: Projects
Status: published
---

This is a list of the projects which I'm directly involved in.


## Code projects

[django-view-decorator](https://github.com/valberg/django-view-decorator)
:   django-view-decorator is decorator aimed at bringing locality of behaviour to the connection between a URL and a view in Django.


## Organisations

[data.coop](https://data.coop)
:   an organisation in Denmark with the aim to provide self-hosted services to its members.


[Django Denmark](https://django-denmark.org)
:   an organisation with the purpose of arranging events about the Django web framework.

[BornHack](https://bornhack.dk)
:   a annual "hacker camp" held on the island of Funen in Denmark

