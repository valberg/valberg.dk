FROM python:3.12-alpine AS build
WORKDIR /srv
RUN pip3 install pelican markdown
COPY . /srv/
RUN pelican content/ -s pelicanconf.py -t theme

FROM nginx:1.27.1-alpine
COPY --from=build /srv/output/ /usr/share/nginx/html/
